//
//  SearchViewModelTests.swift
//  GiphyTests
//
//  Created by Michal on 23.04.22.
//

import XCTest
@testable import Giphy

@MainActor class SearchViewModelTests: XCTestCase {
    typealias SearchViewModel = SearchScreen.SearchViewModel
    typealias SearchFetchingState = SearchScreen.SearchFetchingState
    
    private let enoughNanosecondsToStartTaskButNotComplete: UInt64 = 10_000_000
    
    func testInitialState() {
        let vm = SearchViewModel()
        
        XCTAssertEqual(vm.state, .nothingToSearch)
    }
    
    func testSimpleSearch() async {
        let vm = SearchViewModel()
        vm.query = "Hello"
        
        await vm.search()
        
        guard let _ = vm.searchedGifs.first else {
            XCTFail("Failed to downlaod searched gifs")
            return 
        }
    }
    
    func testSearchingThanChangingQuery() async {
        let vm = SearchViewModel()
        
        // fetch expectectd Gifs
        vm.query = "Hello"
        await vm.search()
        let expectectedGifs = vm.searchedGifs
        
        vm.query = "Hi"
        // run search asynchronously
        Task {
            await vm.search()
        }
        
        // wait a little
         try? await Task.sleep(nanoseconds: enoughNanosecondsToStartTaskButNotComplete)
        
        XCTAssert(vm.state == SearchFetchingState.loading
                  || vm.state == SearchFetchingState.success)
        
        // search for correct gifs
        vm.query = "Hello"
        await vm.search()
        let gifs = vm.searchedGifs
        
        XCTAssertEqual(gifs, expectectedGifs)
    }
    
    /// Tests that different queries give different results.
    func testSearchDifferentQueries() async {
        let vm = SearchViewModel()
        
        vm.query = "Hello"
        await vm.search()
        let helloGifs = vm.searchedGifs
        
        vm.query = "Hi"
        await vm.search()
        let hiGifs = vm.searchedGifs
        
        XCTAssertNotEqual(helloGifs, hiGifs)
    }
    
    func testLoadingNextPage() async {
        let vm = SearchViewModel()
        vm.query = "Hello"
        await vm.search()
        let firstPageGifs = vm.searchedGifs
        
        async let secondPageTask = Task { () -> [Gif] in
            await vm.nextPage()
            return await vm.searchedGifs
        }
        
        try? await Task.sleep(nanoseconds: enoughNanosecondsToStartTaskButNotComplete)
        XCTAssertEqual(vm.state, .loadingNextPage)
        
        
        guard let twoPagesGifs = try? await secondPageTask.result.get() else {
            XCTFail("Failed to fetch second page")
            return
        }
        
        XCTAssertEqual(vm.state, .success)
        
        XCTAssert(twoPagesGifs.count > firstPageGifs.count)
        
    }
}
