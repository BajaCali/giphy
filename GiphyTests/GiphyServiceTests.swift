//
//  GiphyTests.swift
//  GiphyTests
//
//  Created by Michal on 23.04.22.
//

import XCTest
@testable import Giphy

class GiphyServiceTests: XCTestCase {
    
    func testMeasureFetchingTrendingGifs() async {
        let service = GiphyService.shared
        
        measureMetrics([.wallClockTime], automaticallyStartMeasuring: true) {
            let exp = expectation(description: "Finished")
            Task {
                let _ = try? await service.fetch(GiphyRouting.trending())
                await MainActor.run {
                    stopMeasuring()
                }
                exp.fulfill()
            }
            wait(for: [exp], timeout: 20.0)
        }
    }
    
    func testFetchingTrendingGifs() async {
        let service = GiphyService.shared
        
        let gifs = try? await service.fetch(GiphyRouting.trending())
        
        guard let gifs = gifs else {
            XCTFail("No GIFs fetched")
            return
        }

        XCTAssert(gifs.count > 0)
    }
    
    func testTrendingPaging() async {
        let service = GiphyService.shared
        
        async let gifsPageOne = try? service.fetch(GiphyRouting.trending())
        async let gifsPageOneB = try? service.fetch(.trending(page: 0))
        async let gifsPageTwo = try? service.fetch(.trending(page: 1))
        
        
        guard
            let gifsPageOne = await gifsPageOne,
              let gifsPageOneB = await gifsPageOneB,
              let gifsPageTwo = await gifsPageTwo
        else {
            XCTFail("Some gifs failed to fetch")
            return
        }

        XCTAssertNotEqual(gifsPageOne.count, 0)
        XCTAssertNotEqual(gifsPageTwo.count, 0)
        XCTAssertEqual(gifsPageOne, gifsPageOneB)
        XCTAssertNotEqual(gifsPageOne, gifsPageTwo)
    }
}
