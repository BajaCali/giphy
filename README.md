
### Requirements:
- ✅ Use Swift; 
    // of course
- ❌ RxSwift / Combine;
    // I decided to go with MVVM pattern, where SwiftUI updates UI automatically when `@Published` properties change.
- ✅ At least 2 views sourced by data from Giphy;
    // Tranding & Search.
- ✅ Auto search - search requests performed after a minor delay after the user stops typing;
- ❌ Search results are displayed in a UICollectionView;
    // SwiftUI's `LazyVGrid` is used.
- ✅ Loading more results when scrolling;
- ✅ Loading indicators;
- ✅ Unit tests for more important logic (100% coverage not required, just for parts that you would feel like are most important);
    // `GiphyService` and `SearchViewModel` are tested.
- Comments on code that could need an additional explanation;
