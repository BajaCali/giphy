//
//  GiphyService.swift
//  Giphy
//
//  Created by Michal on 13.04.22.
//

import Foundation

enum GiphyError: Error { case badURL, badResponse }

class GiphyService {
    // MARK: - Singleton
    static let shared = GiphyService()
    private init() { }
    
    // MARK: - Private Properties
    private let rootURL = "https://api.giphy.com/v1/gifs"
    private let urlSession = URLSession.shared
    private let jsonDecoder = JSONDecoder()
    
    
    // MARK: - Fetch
    func fetch(_ route: GiphyRouting) async throws -> [Gif] {
        guard let request = route.asRequest(apiURL: rootURL) else {
            throw GiphyError.badURL
        }
        
        let (data, response) = try await urlSession.data(for: request)
        
        guard let httpResponse = response as? HTTPURLResponse,
              200...299 ~= httpResponse.statusCode else {
            throw GiphyError.badResponse
        }
        
        let query = try jsonDecoder.decode(Query.self, from: data)
        
        return query.data
    }
}
