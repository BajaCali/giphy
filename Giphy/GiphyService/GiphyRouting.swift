//
//  GiphyRouting.swift
//  Giphy
//
//  Created by Michal on 12.05.22.
//

import Foundation

enum GiphyRouting: Endpoint {
    case search(query: String, page: Int = 0)
    case trending(page: Int = 0)
}

extension GiphyRouting {
    var path: String {
        switch self {
        case .trending: return "/trending"
        case .search: return "/search"
        }
    }
    
    var headers: [String : String]? { return nil }
    
    var parameters: [String : String?]? {
        var parameters = [String: String]()
        parameters["api_key"] = Constants.giphyApiKey
        parameters["limit"] = "16"
        
        switch self {
        case let .trending(page):
            parameters["offset"] = "\(page)"
        case let .search(query, page):
            parameters["q"] = query
            parameters["offset"] =  "\(page)"
        }
        
        return parameters
    }
}
