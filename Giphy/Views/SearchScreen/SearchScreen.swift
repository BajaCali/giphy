//
//  SearchScreen.swift
//  Giphy
//
//  Created by Michal on 16.04.22.
//

import SwiftUI

struct SearchScreen: View {
    @StateObject var vm: SearchViewModel
    
    init() {
        _vm = StateObject(wrappedValue: SearchViewModel())
    }
    
    var body: some View {
        NavigationView {
            Group {
                switch vm.state {
                case .loading:
                    ProgressView()
                case .nothingToSearch:
                    Text("Search for GIFs")
                case .success, .loadingNextPage, .loadingNextPageFailed:
                    GifsView(gifs: Array(vm.searchedGifs)) {
                        Task {
                            await vm.nextPage()
                        }
                    }
                case .failed:
                    Text("Cannot load gifs.")
                case .noGifsFound:
                    Text("Sorry, did not found any gifs for \(vm.query).")
                }
            }
            .navigationTitle("Search GIFs")
            .searchable(text: $vm.query)
            .onChange(of: vm.query) { _ in
                vm.search()
            }
        }
    }
}

struct SearchScreen_Previews: PreviewProvider {
    static var previews: some View {
        SearchScreen()
    }
}
