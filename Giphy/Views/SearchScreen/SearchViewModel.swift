//
//  SearchViewModel.swift
//  Giphy
//
//  Created by Michal on 17.04.22.
//

import Foundation

extension SearchScreen {
    // MARK: - State
    enum SearchFetchingState: Equatable {
        case loading, success, failed, nothingToSearch, loadingNextPage, loadingNextPageFailed, noGifsFound
    }

    @MainActor class SearchViewModel: ObservableObject {
        // MARK: - Properties
        let giphy = GiphyService.shared
        
        @Published var state = SearchFetchingState.nothingToSearch
        @Published var searchedGifs = [Gif]()
        
        @Published var query = ""
        
        private var page = 0
        
        private var searchTask: Task<(), Never>?
        
        // MARK: - Search
        func search() {
            searchTask?.cancel()
            
            if query.isEmpty {
                state = .nothingToSearch
                return
            }
            
            searchTask = createSearchTask()
        }
        
        // MARK: - Next Page
        func nextPage() async {
            state = .loadingNextPage
            page += 1
            
            let newGifs: [Gif]
            do {
                newGifs = try await giphy.fetch(.search(query: query, page: page))
            } catch {
                state = .loadingNextPageFailed
                return
            }
            
            searchedGifs += newGifs.filter { newGif in
                !searchedGifs.contains(newGif)
            }
            state = .success
        }
        
        // MARK: - Fetch
        private func createSearchTask() -> Task<(), Never> {
            return Task {
                do {
                    try await Task.sleep(nanoseconds: 300_000_000)
                } catch {
                    return
                }
                
                state = .loading
                page = 0
                
                let gifs: [Gif]
                
                do {
                    gifs = try await giphy.fetch(.search(query: query))
                } catch {
                    if Task.isCancelled {
                        state = .nothingToSearch
                    } else {
                        state = .failed
                    }
                    return
                }
                
                guard let _ = gifs.first else {
                    state = .noGifsFound
                    return
                }
                
                searchedGifs = gifs
                state = .success
            }
        }
    }
}
