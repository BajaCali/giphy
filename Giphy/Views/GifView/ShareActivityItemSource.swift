//
//  ShareActivityItemSource.swift
//  Giphy
//
//  Created by Michal on 20.04.22.
//

import Foundation
import UIKit
import LinkPresentation

class ShareActivityItemSource: NSObject, UIActivityItemSource {
    // inspiration: https://gist.github.com/tsuzukihashi/d08fce005a8d892741f4cf965533bd56
    var title: String
    var text: String
    var gif: Data
    
    init(title: String, text: String, gifData: Data) {
        self.title = title
        self.text = text
        self.gif = gifData
        super.init()
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return text
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return gif
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return title
    }

    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        let metadata = LPLinkMetadata()
        metadata.title = title
        metadata.iconProvider = NSItemProvider(object: UIImage(systemName: "photo")!)
        //This is a bit ugly, though I could not find other ways to show text content below title.
        //https://stackoverflow.com/questions/60563773/ios-13-share-sheet-changing-subtitle-item-description
        //You may need to escape some special characters like "/".
        metadata.originalURL = URL(fileURLWithPath: text)
        return metadata
    }

}
