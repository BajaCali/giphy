//
//  GIFView.swift
//  Giphy
//
//  Created by Michal on 15.04.22.
//

import SwiftUI

// MARK: - State
fileprivate enum LoadingState: Equatable {
    case gifLoaded(Data)
    case previewLoaded(Data)
    case loading
    case failed
    case initial
}

// MARK: - View
struct GIFView: View {
    var gif: Gif

    @State private var loadingState = LoadingState.initial
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Color.clear
                switch loadingState {
                case let .gifLoaded(gifData):
                    GIFPlayerView(gifData: gifData)
                        .onTapGesture {
                            showShareSheet(gifData: gifData)
                        }
                case let .previewLoaded(imageData):
                    if let uiImage = UIImage(data: imageData) {
                        Image(uiImage: uiImage)
                            .resizable()
                            .scaledToFit()
                        ProgressView()
                    } else {
                        EmptyView().onAppear {
                            loadingState = .failed
                        }
                    }
                case .loading, .initial:
                    ProgressView()
                case .failed:
                    Text("Sorry. Failed to load.")
                }
            }
        }
        // MARK: - Starting tasks
        .onAppear {
            loadingState = .loading
            // load preview
            let previewTask = Task {
                let data = await downloadData(from: gif.previewURL)
                guard let data = data else { return }
                loadingState = .previewLoaded(data)
            }
            // load gif
            Task {
                let data = await downloadData(from: gif.url)
                guard let data = data else {
                    loadingState = .failed
                    return
                }
                loadingState = .gifLoaded(data)
                previewTask.cancel()
            }
        }
    }

    // MARK: - downloadData
    private func downloadData(from url: URL?) async -> Data? {
        guard let url = url else {
            print("Invalid URL!")
            return nil
        }
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            return data
        } catch {
            print("Failed to download data with error: \(error.localizedDescription)")
        }
        return nil
    }
    
    // MARK: - ShareSheet
    private func showShareSheet(gifData: Data) {
        // if gif is not loaded yet, nothing is presented
        let activityVC = UIActivityViewController(activityItems: [ShareActivityItemSource(title: gif.title, text: "GIF image", gifData: gifData)], applicationActivities: nil)
        
        // https://stackoverflow.com/a/68989580
        UIApplication.shared.connectedScenes
            .filter { $0.activationState == .foregroundActive }
            .first(where: { $0 is UIWindowScene })
            .flatMap({ $0 as? UIWindowScene })?.windows
            .first(where: \.isKeyWindow)?
            .rootViewController?
            .present(activityVC, animated: true, completion: nil)
    }
}

//struct GIFView_Previews: PreviewProvider {
//    static var previews: some View {
//        GIFView()
//    }
//}
