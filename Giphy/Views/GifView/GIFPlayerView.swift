//
//  GifView.swift
//  Giphy
//
//  Created by Michal on 15.04.22.
//

import SwiftUI

public struct GIFPlayerView: UIViewRepresentable {
    private var gifData: Data
    
    public init(gifData: Data) {
        self.gifData = gifData
    }
    
    public func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<GIFPlayerView>) {
        
    }
    
    public func makeUIView(context: Context) -> UIView {
        return GIFPlayerUIView(gifData: gifData)
    }
}
