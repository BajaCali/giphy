//
//  ContentView.swift
//  Giphy
//
//  Created by Michal on 16.04.22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            TrendingScreen()
                .tabItem {
                    Image(systemName: "star.fill")
                    Text("Trending")
                }
            
            SearchScreen()
                .tabItem {
                    Image(systemName: "magnifyingglass")
                    Text("Search GIFs")
                }
        }
    }
}

struct TrendingView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
