//
//  TrendingScreen.swift
//  Giphy
//
//  Created by Michal on 13.04.22.
//

import SwiftUI

// MARK: - State
fileprivate enum TrendingFetchingState { case loading, success, error, loadingNextPage }

struct TrendingScreen: View {
    // MARK: - Properties
    let giphy = GiphyService.shared
    
    @State private var state = TrendingFetchingState.loading
    @State private var trendingGifs = [Gif]()
    
    @State private var page = 0
    
    // MARK: - Body
    var body: some View {
        NavigationView {
            Group {
                switch state {
                case .loading:
                    ProgressView()
                case .success, .loadingNextPage:
                    GifsView(gifs: trendingGifs) {
                        nextPage()
                    }
                case .error:
                    Text("Cannot load gifs.")
                }
            }
            .navigationTitle("Trending Today")
            .task {
                await loadGifs()
            }
        }
    }
    
    // MARK: - Load Gifs
    private func loadGifs(for page: Int = 0) async {
        let gifs: [Gif]
        do {
            gifs = try await giphy.fetch(.trending(page: page))
        } catch {
            print("error fetching gifs: \(error.localizedDescription)")
            if page == 0 {
                state = .error
            }
            return
        }
        
        let uniqueNewGifs = gifs.filter { newGif in
            !trendingGifs.contains(newGif)
        }
        
        Task { @MainActor in
            self.trendingGifs += uniqueNewGifs
            self.state = .success
        }
    }
    
    // MARK: - Paging
    private func nextPage() {
        guard state != .loadingNextPage else { return }
        state = .loadingNextPage
        page += 1
        
        Task {
            await loadGifs(for: page)
        }
    }
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        TrendingScreen()
//    }
//}
