//
//  GifsView.swift
//  Giphy
//
//  Created by Michal on 23.04.22.
//

import SwiftUI

struct GifsView: View {
    let gifs: [Gif]
    var onBottomReached: () -> Void
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView {
                LazyVGrid(columns: [.init(.adaptive(minimum: 175), spacing: 5)], spacing: 5) {
                    ForEach(gifs) { gif in
                        GIFView(gif: gif)
                            .frame(height: (geometry.size.width - 20)  / 3)
                            .onAppear {
                                guard let last = gifs.last else { return }
                                if gif.id == last.id {
                                    onBottomReached()
                                }
                            }
                    }
                }
                .padding(.horizontal, 10)
                ProgressView()
                    .padding()
            }
            .background(background)
        }
    }
    
    private var background: LinearGradient {
        LinearGradient(
            colors: [ // Giphy brand colours with 50 % opacity
                .init(hexValueRGBA: UInt32("80FFF35C", radix: 16)!), // yellow
                .init(hexValueRGBA: UInt32("80FF6666", radix: 16)!), // red
                .init(hexValueRGBA: UInt32("809933FF", radix: 16)!), // purple
                .init(hexValueRGBA: UInt32("8000CCFF", radix: 16)!), // blue
                .init(hexValueRGBA: UInt32("8000FF99", radix: 16)!)  // green
            ],
            startPoint: .topLeading,
            endPoint: .bottomTrailing
        )

    }
}

struct GifsView_Previews: PreviewProvider {
    static var previews: some View {
        GifsView(gifs: []) { }
    }
}
