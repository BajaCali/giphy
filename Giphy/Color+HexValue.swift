//
//  Color+HexValue.swift
//  Giphy
//
//  Created by Michal on 25.04.22.
//

import SwiftUI
import CoreImage

extension Color {
    var hexValue: Int32 {
        let uiColor = UIColor(self)
        let ciColor = CIColor(color: uiColor)
        
        let colorSegment: (Int, Double) -> Int32 = { segment, value in
            Int32(value * 255) << (segment * 8)
        }
        
        return [
            colorSegment(2, ciColor.red),
            colorSegment(1, ciColor.green),
            colorSegment(0, ciColor.blue),
            colorSegment(3, ciColor.alpha)
        ].reduce(0, +)
    }
    
    init(hexValueRGBA: UInt32) {
        let colorSegment: (UInt32) -> Double = { segment in
            let intValue = (hexValueRGBA >> (segment * 8)) & 0xff
            return Double(intValue) / 255
        }
        self = Color(red: colorSegment(2), green: colorSegment(1) , blue: colorSegment(0), opacity: colorSegment(3))
    }
}

