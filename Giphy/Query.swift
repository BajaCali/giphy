//
//  Query.swift
//  Giphy
//
//  Created by Michal on 15.04.22.
//

import Foundation

// MARK: - Query
struct Query: Codable {
    var data: [Gif]
}

// MARK: - Gif
struct Gif: Codable, Identifiable{
    var id: String
    var title: String
    var images: Images
}

extension Gif: Equatable {
    static func ==(rhs: Gif, lhs: Gif) -> Bool {
        lhs.id == rhs.id
    }
}

extension Gif: Hashable {
    func hash(into hasher: inout Hasher) {
           hasher.combine(id)
       }
}

extension Gif {
    var previewURL: URL? {
        URL(string: images.downsized_still.url)
    }
    
    var url: URL? {
        URL(string: images.original.url)
    }
}

// MARK: - Images
struct Images: Codable {
    var original: GifImage
    var downsized_still: GifImage
}

// MARK: - GifImage
struct GifImage: Codable {
    var url: String
    var mp4: String?
}

