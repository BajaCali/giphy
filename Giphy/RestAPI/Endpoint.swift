//
//  Endpoint.swift
//  Giphy
//
//  Created by Michal on 12.05.22.
//

import Foundation

protocol Endpoint {
    var path: String { get }
    var headers: [String: String]? { get }
    var parameters: [String: String?]? { get }
    
}

extension Endpoint {
    func asRequest(apiURL: String) -> URLRequest? {
        guard var urlComponents = URLComponents(string: apiURL) else {
            return nil
        }
        
        urlComponents.path += path
        
        if let parameters = parameters {
            urlComponents.queryItems = parameters.map {
                URLQueryItem(name: $0.key, value: $0.value)
            }
        }
        
        guard let url = urlComponents.url else {
            return nil
        }
        
        var request = URLRequest(url: url)
        
        if let headers = headers {
            _ = headers.map {
                request.addValue($0.key, forHTTPHeaderField: $0.value)
            }
        }
        
        return request
    }
}
